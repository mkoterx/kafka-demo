## Enter the container:  
docker exec -it {containerId} /bin/bash

Find docker host ip from isnide a container: /sbin/ip route|awk '/default/ { print $3 }'  
 
Now you can run all the standar kafka commands

## List topics
```
kafka-topics.sh --list --zookeeper `/sbin/ip route|awk '/default/ { print $3 }'`:2181  
OR
kafka-topics.sh --list --zookeeper zookeeper:2181
```

## Create topic  
```
kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic test
```

## Play yourself
```kafka-console-producer.sh --broker-list localhost:9092 --topic test
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
```
